﻿using Farmacia.COMMON.Interfaces;
using Farmacia.COMMON.Entidades;
using Farmacia.DAL;
using Farmacia.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI
{
    /// <summary>
    /// Lógica de interacción para Datos.xaml
    /// </summary>
    public partial class Datos : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }

        IManejadorEmpleados manejadorEmpleados;
        IManejadorProductos manejadorProductos;
        IManejadorClientes manejadorClientes;
        accion accionEmpleados;
        accion accionProductos;
        accion accionClientes;
        public Datos()
        {
            InitializeComponent();
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorProductos = new ManejadorProductos(new RepositorioDeProductos());
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());

            PonerBotonesEmpleadosEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();

            PonerBotonesProductosEnEdicion(false);
            LimpiarCamposDeProductos();
            ActualizarTablaDeProductos();

            PonerBotonesClientesEnEdicion(false);
            LimpiarCamposDeClientes();
            ActualizarTablaClientes();
        }
        private void ActualizarTablaDeProductos()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductos.Listar;
        }

        private void LimpiarCamposDeProductos()
        {
            txbProductosNombre.Clear();
            txbProductosDescripcion.Clear();
            txbProductosId.Text = "";
            txbProductosPrecioCompra.Clear();
            txbProductosPrecioVenta.Clear();
            txbProductosPresentacion.Clear();

        }

        private void PonerBotonesProductosEnEdicion(bool value)
        {
            btnProductosCancelar.IsEnabled = value;
            btnProductosEditar.IsEnabled = !value;
            btnProductosEliminar.IsEnabled = !value;
            btnProductosGuardar.IsEnabled = value;
            btnProductosNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadosApellidos.Clear();
            txbEmpleadosId.Text = "";
            txbEmpleadosNombre.Clear();
        }

        private void PonerBotonesEmpleadosEnEdicion(bool value)
        {
            btnEmpleadosCancelar.IsEnabled = value;
            btnEmpleadosEditar.IsEnabled = !value;
            btnEmpleadosEliminar.IsEnabled = !value;
            btnEmpleadosGuardar.IsEnabled = value;
            btnEmpleadosNuevo.IsEnabled = !value;
        }
        private void ActualizarTablaClientes()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = manejadorClientes.Listar;
        }

        private void LimpiarCamposDeClientes()
        {
            txbClientesNombre.Clear();
            txbClientesDireccion.Clear();
            txbClientesRFC.Clear();
            txbClientesTelefono.Clear();
            txbClientesCorreo.Clear();
            txbEmpleadosId.Text = "";
        }

        private void PonerBotonesClientesEnEdicion(bool value)
        {
            btnClientesCancelar.IsEnabled = value;
            btnClientesEditar.IsEnabled = !value;
            btnClientesEliminar.IsEnabled = !value;
            btnClientesGuardar.IsEnabled = value;
            btnClientesNuevo.IsEnabled = !value;
        }

        private void btnProductosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            accionProductos = accion.Nuevo;
            PonerBotonesProductosEnEdicion(true);
        }

        private void btnProductosEditar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            accionProductos = accion.Editar;
            PonerBotonesProductosEnEdicion(true);
            Producto p = dtgProductos.SelectedItem as Producto;
            if (p != null)
            {
                txbProductosNombre.Text = p.Nombre;
                txbProductosDescripcion.Text = p.Descripcion;
                txbProductosPrecioCompra.Text = p.PrecioCompra;
                txbProductosPrecioVenta.Text = p.PrecioVenta;
                txbProductosPresentacion.Text = p.Presentacion;
            }
        }

        private void btnProductosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbProductosNombre.Text) || string.IsNullOrEmpty(txbProductosDescripcion.Text) || string.IsNullOrEmpty(txbProductosPrecioCompra.Text) || string.IsNullOrEmpty(txbProductosPrecioVenta.Text) || string.IsNullOrEmpty(txbProductosPresentacion.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (accionProductos == accion.Nuevo)
            {
                Producto p = new Producto()
                {
                    Nombre = txbProductosNombre.Text,
                    Descripcion = txbProductosDescripcion.Text,
                    PrecioCompra = txbProductosPrecioCompra.Text,
                    PrecioVenta=txbProductosPrecioVenta.Text,
                    Presentacion=txbProductosPresentacion.Text
                };
                if (string.IsNullOrEmpty(txbProductosNombre.Text) || string.IsNullOrEmpty(txbProductosDescripcion.Text) || string.IsNullOrEmpty(txbProductosPrecioCompra.Text) || string.IsNullOrEmpty(txbProductosPrecioVenta.Text) || string.IsNullOrEmpty(txbProductosPresentacion.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorProductos.Agregar(p))
                    {
                        MessageBox.Show("Producto agregado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeProductos();
                        ActualizarTablaDeProductos();
                        PonerBotonesProductosEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Producto p = dtgProductos.SelectedItem as Producto;
                p.Nombre = txbProductosNombre.Text;
                p.Descripcion = txbProductosDescripcion.Text;
                p.PrecioCompra = txbProductosPrecioCompra.Text;
                p.PrecioVenta = txbProductosPrecioVenta.Text;
                p.Presentacion = txbProductosPresentacion.Text;
                if (manejadorProductos.Modificar(p))
                {
                    MessageBox.Show("Producto modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    ActualizarTablaDeProductos();
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnProductosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            PonerBotonesProductosEnEdicion(false);
        }

        private void btnPoductosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Producto p = dtgProductos.SelectedItem as Producto;
            if (p != null)
            {
                if (MessageBox.Show("¿Realmente desea eliminar este Producto?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorProductos.Eliminar(p.Id))
                    {
                        MessageBox.Show("Producto Eliminado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeProductos();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnEmpleadosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(true);
            accionEmpleados = accion.Nuevo;
        }

        private void btnEmpleadosEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                txbEmpleadosId.Text = emp.Id;
                txbEmpleadosApellidos.Text = emp.Apellidos;
                txbEmpleadosNombre.Text = emp.Nombre;
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadosEnEdicion(true);
            }
        }

        private void btnEmpleadosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbEmpleadosNombre.Text) || string.IsNullOrEmpty(txbEmpleadosApellidos.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    Nombre = txbEmpleadosNombre.Text,
                    Apellidos = txbEmpleadosApellidos.Text,
                };
                if (string.IsNullOrEmpty(txbEmpleadosNombre.Text) || string.IsNullOrEmpty(txbEmpleadosApellidos.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorEmpleados.Agregar(emp))
                    {
                        MessageBox.Show("Empleado agregado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeEmpleados();
                        ActualizarTablaEmpleados();
                        PonerBotonesEmpleadosEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Empleado no se pudo agregar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Empleado emp = dtgEmpleados.SelectedItem as Empleado;
                emp.Apellidos = txbEmpleadosApellidos.Text;
                emp.Nombre = txbEmpleadosNombre.Text;
                if (manejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEmpleadosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(false);
        }

        private void btnEmpleadosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnClientesNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            accionClientes = accion.Nuevo;
            PonerBotonesClientesEnEdicion(true);
        }

        private void btnClientesEditar_Click(object sender, RoutedEventArgs e)
        {
            Cliente c = dtgClientes.SelectedItem as Cliente;
            if (c != null)
            {
                txbClientesId.Text = c.Id;
                txbClientesNombre.Text = c.Nombre;
                txbClientesDireccion.Text = c.Direccion;
                txbClientesRFC.Text = c.RFC;
                txbClientesTelefono.Text = c.Telefono;
                txbClientesCorreo.Text = c.Correo;
                accionClientes = accion.Editar;
                PonerBotonesClientesEnEdicion(true);
            }
        }

        private void btnClientesGuardar_Click(object sender, RoutedEventArgs e)
        {

            if (accionClientes == accion.Nuevo)
            {
                Cliente c = new Cliente()
                {
                    Nombre = txbClientesNombre.Text,
                    Direccion = txbClientesDireccion.Text,
                    RFC=txbClientesRFC.Text,
                    Telefono=txbClientesTelefono.Text,
                    Correo=txbClientesCorreo.Text
                };
                if (string.IsNullOrEmpty(txbClientesNombre.Text) || string.IsNullOrEmpty(txbClientesDireccion.Text) || string.IsNullOrEmpty(txbClientesRFC.Text) || string.IsNullOrEmpty(txbClientesTelefono.Text) || string.IsNullOrEmpty(txbClientesCorreo.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorClientes.Agregar(c))
                    {
                        MessageBox.Show("Cliente registrado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeClientes();
                        ActualizarTablaClientes();
                        PonerBotonesClientesEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("No se pudo registrar al Cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Cliente c = dtgClientes.SelectedItem as Cliente;
                c.Nombre = txbClientesNombre.Text;
                c.Direccion = txbClientesDireccion.Text;
                c.RFC = txbClientesRFC.Text;
                c.Telefono = txbClientesTelefono.Text;
                c.Correo = txbClientesCorreo.Text;
                if (manejadorClientes.Modificar(c))
                {
                    MessageBox.Show("Cliente modificado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();
                    ActualizarTablaClientes();
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Cliente no se pudo actualizar", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnClientesCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(false);
        }

        private void btnClientesEliminar_Click(object sender, RoutedEventArgs e)
        {
            Cliente c = dtgClientes.SelectedItem as Cliente;
            if (c != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este cliente?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorClientes.Eliminar(c.Id))
                    {
                        MessageBox.Show("Cliente eliminado", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaClientes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el cliente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }
    }
}