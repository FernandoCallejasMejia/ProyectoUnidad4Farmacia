﻿using Farmacia.COMMON.Interfaces;
using Farmacia.COMMON.Entidades;
using Farmacia.DAL;
using Farmacia.BIZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Farmacia.GUI
{
    /// <summary>
    /// Lógica de interacción para Ventas.xaml
    /// </summary>
    public partial class Ventas : Window
    {
        enum Accion
        {
            Nuevo,
            Editar
        }
        ManejadorTickets manejadorDeTickets;
        ManejadorEmpleados manejadorEmpleados;
        ManejadorProductos manejadorProductos;
        ManejadorClientes manejadorClientes;
        Ticket ticket;

        Accion accionTicket;
        public Ventas()
        {
            InitializeComponent();
            manejadorDeTickets = new ManejadorTickets(new RepositorioDeTickets());
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorProductos = new ManejadorProductos(new RepositorioDeProductos());
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());


            ActualizarTablaDeTickets();
            gridVenta.IsEnabled = false;
        }
        private void ActualizarTablaDeTickets()
        {
            dtgTickets.ItemsSource = null;
            dtgTickets.ItemsSource = manejadorDeTickets.Listar;
        }
        private void ActualizarListaDeProductosEnTicket()
        {
            dtgProductosEnTicket.ItemsSource = null;
            dtgProductosEnTicket.ItemsSource = ticket.ProductosVendidos;
        }
        private void ActualizarCombosVenta()
        {
            cmbEmpleado.ItemsSource = null;
            cmbEmpleado.ItemsSource = manejadorEmpleados.Listar;

            cmbProductos.ItemsSource = null;
            cmbProductos.ItemsSource = manejadorProductos.Listar;

            cmbCliente.ItemsSource = null;
            cmbCliente.ItemsSource = manejadorClientes.Listar;
        }
        private void LimpiarCamposDeTicket()
        {
            lblFechaHoraVenta.Content = "";
            dtgProductosEnTicket.ItemsSource = null;
            cmbEmpleado.ItemsSource = null;
            cmbCliente.ItemsSource = null;
            cmbProductos.ItemsSource = null;
            txbCantidad.Text = null;
            txbPrecio.Text = null;
            txbTotal.Text = null;
        }

        private void btnNuevoTicket_Click(object sender, RoutedEventArgs e)
        {
            gridVenta.IsEnabled = true;
            ActualizarCombosVenta();
            btnGenerarTicket.IsEnabled = false;
            ticket = new Ticket();
            ticket.ProductosVendidos = new List<Producto>();
            ActualizarListaDeProductosEnTicket();
            accionTicket = Accion.Nuevo;
        }

        private void btnEliminarTicket_Click(object sender, RoutedEventArgs e)
        {
            Ticket t = dtgTickets.SelectedItem as Ticket;
            if (t != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar el registro del Ticket?", "Farmacia", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeTickets.Eliminar(t.Id))
                    {
                        MessageBox.Show("Eliminado con éxito", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeTickets();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnGenerarTicket_Click(object sender, RoutedEventArgs e)
        {
            if (accionTicket == Accion.Nuevo)
            {
                ticket.Vendedor = cmbEmpleado.SelectedItem as Empleado;
                ticket.Comprador = cmbCliente.SelectedItem as Cliente;
                ticket.FechaHoraVenta = DateTime.Now;
                ticket.Cantidad = int.Parse(txbCantidad.Text);
                ticket.Precio = float.Parse(txbPrecio.Text);
                ticket.Total = float.Parse(txbTotal.Text);
                lblFechaHoraVenta.Content = DateTime.Now;
                ArchivoTicket venta = new ArchivoTicket();
                if (venta.GenerarTicket(cmbCliente.Text, cmbProductos.Text, float.Parse(txbPrecio.Text), int.Parse(txbCantidad.Text), float.Parse(txbTotal.Text), cmbEmpleado.Text, cmbCliente.Text + ".txt"))
                {
                    if (manejadorDeTickets.Agregar(ticket))
                    {

                        MessageBox.Show("Ticket generado correctamente", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        LimpiarCamposDeTicket();
                        gridVenta.IsEnabled = false;
                        ActualizarTablaDeTickets();
                    }
                    else
                    {
                        MessageBox.Show("Error al generar el Ticket", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                ticket.Vendedor = cmbEmpleado.SelectedItem as Empleado;
                ticket.Comprador = cmbCliente.SelectedItem as Cliente;
                ticket.FechaHoraVenta = DateTime.Now;
                ticket.Cantidad = int.Parse(txbCantidad.Text);
                ticket.Precio = float.Parse(txbPrecio.Text);
                ticket.Total = float.Parse(txbTotal.Text);
                lblFechaHoraVenta.Content = DateTime.Now;
                if (manejadorDeTickets.Modificar(ticket))
                {
                    MessageBox.Show("Ticket generado correctamente", "Farmacia", MessageBoxButton.OK,MessageBoxImage.Information);
                    LimpiarCamposDeTicket();
                    gridVenta.IsEnabled = false;
                    ActualizarTablaDeTickets();
                }
                else
                {
                    MessageBox.Show("Error al generar el Ticket", "Farmacia", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCalcularTotal_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txbCantidad.Text) || string.IsNullOrEmpty(txbPrecio.Text) || string.IsNullOrEmpty(cmbEmpleado.Text) || string.IsNullOrEmpty(cmbCliente.Text) || string.IsNullOrEmpty(cmbProductos.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show("Listo", "Venta", MessageBoxButton.OK, MessageBoxImage.Information);
                Ticket venta = new Ticket();
                venta.Cantidad = int.Parse(txbCantidad.Text);
                venta.Precio = float.Parse(txbPrecio.Text);
                txbTotal.Text = venta.CalcularTotal().ToString();
                btnGenerarTicket.IsEnabled = true;
                Producto p = cmbProductos.SelectedItem as Producto;
                if (p != null)
                {
                    ticket.ProductosVendidos.Add(p);
                    ActualizarListaDeProductosEnTicket();
                }
            }
        }

        private void btnCancelarTicket_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeTicket();
            gridVenta.IsEnabled = false;
        }

        private void dtgTickets_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Ticket t = dtgTickets.SelectedItem as Ticket;
            if (t != null)
            {
                gridVenta.IsEnabled = true;
                ticket = t;
                //ActualizarListaDeProductosEnTicket();
                accionTicket = Accion.Editar;
                lblFechaHoraVenta.Content = ticket.FechaHoraVenta.ToString();
                ActualizarCombosVenta();
                cmbEmpleado.Text = ticket.Vendedor.ToString();
                cmbCliente.Text = ticket.Comprador.ToString();
                cmbProductos.Text = ticket.ProductosVendidos.ToString();
                txbCantidad.Text = ticket.Cantidad.ToString();
                txbPrecio.Text = ticket.Precio.ToString();
                btnGenerarTicket.IsEnabled = true;
            }
        }
    }
}
